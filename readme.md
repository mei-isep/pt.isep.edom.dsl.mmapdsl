## Instructions

This is a Maven/Tycho Xtext Eclipse multi-module project.

All these projects were initially generate by the Xtext Wizard based on the ecore metamodel of the [Mind Map Modeling Project](https://bitbucket.org/mei-isep/pt.isep.edom.mindmap)

This folder contains the "parent" project, which simply "includes" all the other modules/projects.

To open all the projects in Eclipse use "File/Import.../Existing Projects into Workspace". Select the folder of the "parent" project (i.e., the one that contains this file). Check "Search for nested projects". 

**Note:** The pt.isep.edom.dsl.mmapdsl references the [Mind Map Modeling Project](https://bitbucket.org/mei-isep/pt.isep.edom.mindmap) so you should also import this project into the workspace. 

The projects are:

- pt.isep.edom.dsl.mmapdsl.parent
- pt.isep.edom.dsl.mmapdsl
- pt.isep.edom.dsl.mmapdsl.ide
- pt.isep.edom.dsl.mmapdsl.ui
- pt.isep.edom.dsl.mmapdsl.target
- pt.isep.edom.dsl.mmapdsl.tests
- pt.isep.edom.dsl.mmapdsl.ui.tests

## Source Control

There is a .gitignore file in the root folder that ignores all the folders that Xtext will generate. These are:

- src-gen
- xtend-gen

Also, *bin* and *target* are ignored.

## Furhter Instructions

You can find further instructions in the readme.md file of the [pt.isep.edom.dsl.mmapdsl Project](pt.isep.edom.dsl.mmapdsl/).
 