package pt.isep.edom.dsl;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowContext;

public class ResourceWriter extends WorkflowComponentWithSlot {
	private String uri;

	public void invoke(IWorkflowContext ctx) {
		Resource resource = (Resource) ctx.get(getSlot());
		URI uri = URI.createFileURI(getUri());
		uri = resource.getResourceSet().getURIConverter().normalize(uri);
		resource.setURI(uri);
		try {
			resource.save(null);
		} catch (IOException e) {
			throw new WrappedException(e);
		}
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUri() {
		return uri;
	}
}
