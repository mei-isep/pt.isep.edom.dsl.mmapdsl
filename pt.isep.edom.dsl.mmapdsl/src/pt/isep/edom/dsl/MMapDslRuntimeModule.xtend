/*
 * generated by Xtext 2.12.0
 */
package pt.isep.edom.dsl

import org.eclipse.xtext.conversion.IValueConverterService
import pt.isep.edom.dsl.converter.MMapDslValueConverter

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class MMapDslRuntimeModule extends AbstractMMapDslRuntimeModule {
	
    override
    public Class<? extends IValueConverterService> bindIValueConverterService() {
    	// return null
        return MMapDslValueConverter 
    }	
}
