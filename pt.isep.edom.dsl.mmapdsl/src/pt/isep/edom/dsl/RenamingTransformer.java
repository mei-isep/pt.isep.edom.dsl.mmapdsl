package pt.isep.edom.dsl;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowContext;

import pt.isep.edom.mindmap.mindmap.Topic;

public class RenamingTransformer extends WorkflowComponentWithSlot {
	private boolean toLowerCase = false;

	public void invoke(IWorkflowContext ctx) {
		Resource resource = (Resource) ctx.get(getSlot());
		
		TreeIterator<EObject> i = resource.getAllContents();
		while (i.hasNext()) {
			EObject o=i.next();
			if (o instanceof Topic) {
				Topic t=(Topic)o;
				t.setName(isToLowerCase() ? t.getName().toLowerCase() : t.getName().toUpperCase());
			}
		}
	}

	public void setToLowerCase(boolean toLowerCase) {
		this.toLowerCase = toLowerCase;
	}

	public boolean isToLowerCase() {
		return toLowerCase;
	}
}