package pt.isep.edom.dsl;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import pt.isep.edom.mindmap.mindmap.Map;

public class StandaloneRun {

	/**
	 * 
	 * @param args
	 * 
	 * see: https://typefox.io/how-and-why-use-xtext-without-the-ide
	 * see: http://www.davehofmann.de/different-ways-of-parsing-with-xtext/
	 * see: https://www.eclipse.org/forums/index.php/t/1089475/
	 */
	public static void main(String[] args) {
		// do this only once per application
		Injector injector = new MMapDslStandaloneSetup().createInjectorAndDoEMFRegistration();
		 
		// obtain a resourceset from the injector
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		 
		// load a resource by URI, in this case from the file system
		Resource resource = resourceSet.getResource(URI.createFileURI("dsl/test2.mmapdsl"), true);
		
		try {
			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);
			//Map myMap=(Map)root;
			
			System.out.println(">>>>>>>>>>>");
			System.out.println(root.toString());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}
