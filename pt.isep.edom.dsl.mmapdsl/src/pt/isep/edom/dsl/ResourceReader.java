package pt.isep.edom.dsl;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowContext;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import pt.isep.edom.mindmap.mindmap.MindmapFactory;
import pt.isep.edom.mindmap.mindmap.MindmapPackage;

public class ResourceReader extends WorkflowComponentWithSlot {
	private String uri;

	public void invoke(IWorkflowContext ctx) {
		// do this only once per application
		Injector injector = new MMapDslStandaloneSetup().createInjectorAndDoEMFRegistration();

		// obtain a resourceset from the injector
		XtextResourceSet resSet = injector.getInstance(XtextResourceSet.class);

		// Initialize the model
		MindmapPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
		MindmapFactory factory = MindmapFactory.eINSTANCE;

		// ResourceSet resourceSet = new ResourceSetImpl();
		URI fileURI = URI.createFileURI(uri);
		Resource resource = resSet.getResource(fileURI, true);
		ctx.put(getSlot(), resource);
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUri() {
		return uri;
	}
}
