## 1. Dependency Between the Xtext Project and the EMF Modeling Project	

In the MANIFEST.MF file of the Xtext Project there should be a dependency to the EMF Modeling Project so that Xtext is able to locate the metamodel and its supporting code and other artifacts (e.g., gen file). The Wizard "Xtext Project from Existing Ecore Models" will do this automatically.

## 2. Code added Manually

### 2.1 Value Converters

Added class MMapDslValueConverter for dealing with Date representations.

Setup of MMapDslValueConverter in MMapDslRuntimeModule.

### 2.2 Formatters

See the code in the Xtend class MMapDslFormatter.

### 2.3 Using EMF API with Xtext Textual Representation of Models

There are examples that use the EMF API and the textual concrete representation of models.

See classes:	

	SaveModel
	
Regular Main java class that illustrates how to create a memory in memory and then save it in the textual representation.
	
	GenerateDot

Regular Main java class that illustrates how to load textual representation of a model into memory then traverse it in order to generate a PlantUML diagram.

	pt.isep.edom.dsl.Renamer
	
This is an example of e MWE2 worflow that reads, transforms and saves a textual instance of a model.	 


## 3. Running standalone examples with maven from the command line


### 3.1 Building all the Xtext Projects

In the root folder of project "parent" type:

	mvn clean install

### 3.2 Execute Code Outside Eclipse 

The project uses Maven/Tycho. 

See: https://stackoverflow.com/questions/9846046/run-main-class-of-maven-project

In the root folder of project "with the grammar" (i.e., the project with the xtext file) type:

	mvn exec:java@saveModel
	
The previous command will execute "exactly" the same code as the similar class in the **pt.isep.edom.mindmap** project but now the model is saved using the textual representation of the DSL. 	

	mvn exec:java@generateDot
	
The previous command will execute "exactly" the same code as the similar class in the **pt.isep.edom.mindmap** project but now the input model is using the textual representation of the DSL. 		
	
	mvn exec:java@mwe2Launcher
		
This last command will execute the workflow file "/src/pt/isep/edom/dsl/GenerateMMapDsl.mwe2" that generates all the code based on the file MMapDsl.xtext

	mvn exec:java@mwe2Renamer
		
This last command will execute the workflow file "/src/pt/isep/edom/dsl/Renamer.mwe2" that that reads, transforms and saves a textual instance of a model.

	mvn exec:java@standaloneRun 
	
This last example is not yet completed.



